<?php
/**
 * Created by PhpStorm.
 * User: sasoffice
 * Date: 2019-03-31
 * Time: 16:30
 */
?>
<div class="row">
    <div class="col-md-12">
        <h2>Students CRUD
            <div class="pull-right">
                <a class="btn btn-info" href="<?php echo base_url('records/create') ?>"> Add New Student</a>
            </div>
        </h2>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Class Room</th>
            <th>Teachers Name</th>
            <th>Student Firstname</th>
            <th>Student Lastname</th>
            <th>Gender</th>
            <th>Joined Year</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $d) { ?>
            <tr>
                <td><?php echo $d->class_room; ?></td>
                <td><?php echo $d->teachers_name; ?></td>
                <td><?php echo $d->student_firstname; ?></td>
                <td><?php echo $d->student_lastname; ?></td>
                <td><?php echo $d->gender; ?></td>
                <td><?php echo $d->joined_year; ?></td>
                <td>
                    <form method="DELETE" action="<?php echo base_url('records/delete/'.$d->id); ?>">
                        <a class="btn btn-info btn-xs" href="<?php echo base_url('records/edit/'.$d->id) ?>"><i style="color: #ffffff; size: 64px;" class="fas fa-pen-square"></i></a>
                        <button type="submit" class="btn btn-danger btn-xs"><i style="color: #ffffff; size: 64px;" class="fas fa-user-times"></i></button>
                    </form>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
