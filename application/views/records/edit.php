<?php
/**
 * Created by PhpStorm.
 * User: sasoffice
 * Date: 2019-03-31
 * Time: 18:03
 */
?>
<?php echo validation_errors(); ?>
<form method="post" action="<?php echo base_url('records/update/'. $record->id);?>">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Class Room</label>
                <div class="col-md-9">
                    <input type="text" name="title" class="form-control" value="<?php echo $record->class_room; ?>">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Teachers Name</label>
                <div class="col-md-9">
                    <textarea name="description" class="form-control"><?php echo $record->teachers_name; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Student Firstname</label>
                <div class="col-md-9">
                    <textarea name="description" class="form-control"><?php echo $record->student_firstname; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Student Lastname</label>
                <div class="col-md-9">
                    <textarea name="description" class="form-control"><?php echo $record->student_lastname; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Gender</label>
                <div class="col-md-9">
                    <textarea name="description" class="form-control"><?php echo $record->gender; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Joined Year</label>
                <div class="col-md-9">
                    <textarea name="description" class="form-control"><?php echo $record->joined_year; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 pull-right">
            <input type="submit" name="Save" class="btn btn-secondary">
        </div>
    </div>

</form>
