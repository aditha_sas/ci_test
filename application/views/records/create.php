<?php
/**
 * Created by PhpStorm.
 * User: sasoffice
 * Date: 2019-03-31
 * Time: 16:31
 */
?>
<?php echo validation_errors(); ?>
<form method="post" action="<?php echo base_url('records/store');?>">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Class Room</label>
                <div class="col-md-9">
                    <input type="text" name="class_room" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Teachers Name</label>
                <div class="col-md-9">
                    <input type="text" name="teachers_name" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Student Firstname</label>
                <div class="col-md-9">
                    <input type="text" name="student_firstname" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Student Lastname</label>
                <div class="col-md-9">
                    <input type="text" name="student_lastname" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Gender</label>
                <div class="col-md-9">
                    <input type="text" name="gender" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group">
                <label class="col-md-3">Joined Year</label>
                <div class="col-md-9">
                    <input type="text" name="joined_year" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2 pull-right">
            <input type="submit" name="Save" class="btn btn-secondary">
        </div>
    </div>

</form>
