<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Records extends CI_Controller
{
    public function __construct()
    {
        //load database in autoload libraries
        parent::__construct();
        $this->load->database();
        $this->load->model('RecordsModel');


    }

    public function index()
    {
        $records=new RecordsModel;
        $data['data']=$records->get_records();
        $this->load->view('includes/header');
        $this->load->view('records/list',$data);
        $this->load->view('includes/footer');
    }

    public function create()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->view('includes/header');
        $this->load->view('records/create');
        $this->load->view('includes/footer');
    }

    public function store()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('class_room','Class Room','required|exact_length[1]');
        $this->form_validation->set_rules('gender','Gender','required|exact_length[1]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('includes/header');
            $this->load->view('records/create');
            $this->load->view('includes/footer');
        } else {
            $records=new RecordsModel;
            $records->insert_record();
            redirect(base_url('records'));
        }

    }
//
    public function edit($id)
    {
        $this->load->helper(array('form', 'url'));
        $record = $this->db->get_where('student_records', array('id' => $id))->row();
        $this->load->view('includes/header');
        $this->load->view('records/edit',array('record'=>$record));
        $this->load->view('includes/footer');
    }
//
    public function update($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('class_room','Class Room','required|exact_length[1]');
        $this->form_validation->set_rules('gender','Gender','required|exact_length[1]');
        if ($this->form_validation->run() == FALSE)
        {
            $record = $this->db->get_where('student_records', array('id' => $id))->row();
            $this->load->view('includes/header');
            $this->load->view('records/edit',array('record'=>$record));
            $this->load->view('includes/footer');
        } else {
            $records=new RecordsModel;
            $records->update_record($id);
            redirect(base_url('records'));
        }
    }
//
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student_records');
        redirect(base_url('records'));
    }

}
?>