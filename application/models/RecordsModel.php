<?php
/**
 * Created by PhpStorm.
 * User: sasoffice
 * Date: 2019-03-31
 * Time: 16:05
 */

class RecordsModel extends CI_Model
{
    public function get_records()
    {
        $query = $this->db->get("student_records");
        return $query->result();
    }

    public function insert_record()
    {
        $data = array(
            'class_room' => $this->input->post('class_room'),
            'teachers_name'=> $this->input->post('teachers_name'),
            'student_firstname'=> $this->input->post('student_firstname'),
            'student_lastname'=> $this->input->post('student_lastname'),
            'gender'=> $this->input->post('gender'),
            'joined_year'=> $this->input->post('joined_year')
        );
        return $this->db->insert('student_records', $data);
    }

    public function update_record($id)
    {
        $data=array(
            'class_room' => $this->input->post('class_room'),
            'teachers_name'=> $this->input->post('teachers_name'),
            'student_firstname'=> $this->input->post('student_firstname'),
            'student_lastname'=> $this->input->post('student_lastname'),
            'gender'=> $this->input->post('gender'),
            'joined_year'=> $this->input->post('joined_year')
        );
        if ($id==0) {
            return $this->db->insert('student_records',$data);
        } else {
            $this->db->where('id',$id);
            return $this->db->update('student_records',$data);
        }
    }

}

?>